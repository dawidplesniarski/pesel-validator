package pl.edu.pwsztar

import spock.lang.Specification

class IsCorrectSpec  extends Specification{

    def "should check if pesel is valid"() {
        given:
        UserId user = new UserId("44051401359");
        UserId user2 = new UserId("44051401358");
        UserId user3 = new UserId("98051401358");
        when:
        boolean isValid = user.isCorrect();
        boolean isNotValid = user2.isCorrect();
        boolean notValid = user3.isCorrect();
        then:
        isValid
        !isNotValid
        !notValid
    }
}
